<?php

namespace App\Models;


use App\Traits\SysCallTrait;

class CpuModel
{
    use SysCallTrait;

    public function getState()
    {
        $f = $this->getSysCallResult('sensors');
        preg_match('#CPU Temperature\\:\s+\\+([0-9\\.]+)#s', $f, $preg);
        return $preg[1];
    }
}