<?php

namespace App\Models;


use App\Traits\SysCallTrait;

class ProcessesModel
{
    use SysCallTrait;

    public function getState()
    {
        return round(trim($this->getSysCallResult('ps -e|wc -l')));
    }
}