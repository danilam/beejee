<?php

namespace App\Models;


use App\Traits\SysCallTrait;

class ServerTimeModel
{
    use SysCallTrait;

    public function getState()
    {
        return date('Y-m-d H:i:s');
    }
}