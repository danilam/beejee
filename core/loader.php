<?php
chdir(__DIR__);
define('APP_PATH', dirname(__DIR__) . DIRECTORY_SEPARATOR);
ini_set('display_errors', 1);
/**
 * Autoloader
 */
spl_autoload_register(function ($class) {
    $file = str_replace('\\', DIRECTORY_SEPARATOR, $class) . '.php';
    if (strpos($file, 'App' . DIRECTORY_SEPARATOR) === 0) {
        $file = str_replace('App' . DIRECTORY_SEPARATOR, 'core' . DIRECTORY_SEPARATOR, $file);
    }
    // if the file exists, require it
    if (file_exists(APP_PATH . $file)) {
        require APP_PATH . $file;
    }
});