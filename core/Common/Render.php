<?php

namespace App\Common;

/**
 * Class Template
 * Displays templates
 * @package App\Common
 */
class Render
{
    var $title = '';
    var $description = '';
    var $pageTitle = '';

    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    public function setPageTitle($pageTitle)
    {
        $this->pageTitle = $pageTitle;
        return $this;
    }

    public function render($container, $include = '', $vars = [])
    {
        if (!empty($included_file)) {
            $included_file = APP_PATH . 'core' . DIRECTORY_SEPARATOR . 'Views' . DIRECTORY_SEPARATOR . $include . '.php';
        }

        extract($vars);
        $container_file = APP_PATH . 'core' . DIRECTORY_SEPARATOR . 'Views' . DIRECTORY_SEPARATOR . $container . '.php';
        if (file_exists($container_file)) {
            require($container_file);
        } else {
            throw new \Exception('Template ' . $container_file . ' not found');
        }

    }
}