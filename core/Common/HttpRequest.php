<?php

namespace App\Common;

/**
 * Class HttpRequest
 * Implements simple request parsing
 * @package App\Common
 */
class HttpRequest
{
    var $uri = '';
    var $method = '';
    var $post = [];
    var $get = [];

    public function __construct()
    {
        if (empty($_SERVER)) {
            $this->uri = null;
            $this->method = null;
            return;
        }

        if (!empty($_SERVER['REQUEST_URI'])) {
            list($this->uri) = explode('?', $_SERVER['REQUEST_URI']);
            $this->get = $_GET;
        }

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $this->post = $_POST;
        }

        //Remove last / in URI
        if ($this->uri && $this->uri[strlen($this->uri) - 1] == '/') {
            $this->uri = substr($this->uri, 0, strlen($this->uri) - 1);
        }

    }
}