<?php

namespace App\Common;

/**
 * Class Router
 * Implements simple routing
 * @package App\Common
 */
class Router
{
    /**
     * @var HttpRequest
     */
    var $request;

    /**
     * Router constructor.
     */
    public function __construct()
    {
        $this->request = new HttpRequest();
    }

    /**
     * GET
     * @param $uri
     * @param \Closure $function
     */
    public function get($uri, \Closure $function)
    {
        if ($this->request->method != 'GET') {
            return;
        }
        $this->process($uri, $function);
    }

    /**
     * Process request
     * @param $uri
     * @param \Closure $function
     */
    private function process($uri, \Closure $function)
    {
        //If URI matches, run function and exit
        if ($uri == $this->request->uri) {
            $function();
            exit(0);
        }
    }

    /**
     * Any request type
     * @param $uri
     * @param \Closure $function
     */
    public function any($uri, \Closure $function)
    {
        $this->process($uri, $function);
    }

    /**
     * POST
     * @param $uri
     * @param \Closure $function
     */
    public function post($uri, \Closure $function)
    {
        if ($this->request->method != 'POST') {
            return;
        }
        $this->process($uri, $function);
    }
}