<?php

namespace App\Common;


use App\Controllers\AjaxController;
use App\Controllers\HomeController;

/**
 * Class Application
 * Main class
 * @package App\Common
 */
class Application
{

    protected static $app;

    /**
     * @var Router
     */
    var $router;

    var $render;

    /**
     * Application constructor.
     */
    protected function __construct()
    {
        $this->router = new Router();
        $this->render = new Render();
    }

    /**
     * @return Application
     */
    public static function getInstance()
    {
        if (empty(static::$app)) {
            static::$app = new Self();
        }
        return static::$app;
    }

    /**
     * Simple routing
     */
    public function route()
    {
        $this->router->any('', function () {
            (new HomeController())->homeView();
        });
        $this->router->any('/ajax/server-time', function () {
            (new AjaxController())->serverTime();
        });
        $this->router->any('/ajax/cpu-temp', function () {
            (new AjaxController())->cpuTemperature();
        });
        $this->router->any('/ajax/proc-count', function () {
            (new AjaxController())->processesCounter();
        });
    }
}