<?php

namespace App\Traits;


trait SysCallTrait
{
    private function getSysCallResult($cmd)
    {
        ob_start();
        system($cmd);
        $f = ob_get_contents();
        ob_end_clean();
        return $f;
    }
}