<?php

namespace App\Controllers;


use App\Common\Application;

/**
 * Class HomeController
 * Simple controller
 * @package App\Controllers
 */
class HomeController
{
    public function homeView()
    {
        Application::getInstance()->render->setTitle('Home page')
            ->setPageTitle('Hello BeeJee')
            ->render('base', 'home');
    }
}