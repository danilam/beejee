<?php

namespace App\Controllers;

use App\Models\CpuModel;
use App\Models\ProcessesModel;
use App\Models\ServerTimeModel;

/**
 * Class AjaxController
 * Implements AJAX requests
 * @package App\Controllers
 */
class AjaxController
{

    public function serverTime()
    {
        $this->toJson([
            'message' => (new ServerTimeModel())->getState()
        ]);
    }

    private function toJson($data)
    {
        header('Content-type: application/json');
        echo json_encode($data, JSON_UNESCAPED_UNICODE);
    }

    public function cpuTemperature()
    {
        $this->toJson([
            'message' => (new CpuModel())->getState()
        ]);
    }

    public function processesCounter()
    {
        $this->toJson([
            'message' => (new ProcessesModel())->getState()
        ]);
    }
}