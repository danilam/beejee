<!doctype html>
<html lang="en">
<head>
    <title><?= $this->title; ?></title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <!-- My Own CSS -->
    <link rel="stylesheet" href="css/style.css">
    <script src="js/jquery-3.3.1.js"></script>
</head>
<body>
<div class="wrapper">
    <div class="container">
        <div class="my-panel">
            <h1><?= $this->pageTitle; ?></h1>
            <div class="container" id="indicators"></div>
        </div>
    </div>
</div>

<script>

    let indicators = {
        timers: [],
        add: function (title, method, id, rate) {
            var h = '<div class="row">\n' +
                '<div class="col col-sm-2">' + title + ':</div>\n' +
                '<div class="col col-sm-2"><span id="' + id + '"></span></div>\n' +
                '<div class="col col-sm-2"><span id="' + id + '_counter"></span></div>\n' +
                '</div>\n';
            $(h).appendTo('#indicators');
            this.timers.push({
                method: method,
                id: id,
                rate: 0,
                defaultRate: rate,
                locked: false,
            });
        },
        run: function () {
            for (let i = 0; i < this.timers.length; i++) {
                this.timers[i].rate -= 0.5;
                if (this.timers[i].rate >= 0) {
                    $('#' + this.timers[i].id + '_counter').html(String('.').repeat(1 + this.timers[i].rate * 2));
                    continue;
                }
                this.timers[i].rate = this.timers[i].defaultRate;
                $('#' + this.timers[i].id + '_counter').html(String('.').repeat(1 + this.timers[i].rate * 2));
                displayIndicator(this.timers[i].method, this.timers[i].id, i)
            }
        }
    }


    function displayIndicator(_method, _div, _obj) {
        var obj = indicators.timers[_obj];
        console.log(obj.locked)
        if (obj.locked) {
            return;
        }
        obj.locked = true
        $.ajax({
            url: 'ajax/' + _method,
            cache: false,
            dataType: 'json',
            type: 'POST',
            data: {
                nc: Math.random()
            },
            success: function (data) {
                $('#' + _div).html(data.message);
            },
            error: function () {
                //для демо сойдет и так:)
            },
            complete: function () {
                obj.locked = false
                //тут тоже что-нибудь можно сделать
                //например, скрыть показанный спиннер
            }
        });

    }

    $(function () {
        indicators.add('Server time', 'server-time', 'server_time', 2);
        indicators.add('CPU temperature', 'cpu-temp', 'cpu_temp', 3);
        indicators.add('Processes', 'proc-count', 'proc_count', 5);

        window.setInterval(function () {
            indicators.run()
        }, 500);

    });

</script>
</body>
</html>